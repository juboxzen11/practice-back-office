import React from 'react'
import { CircularProgress } from '@mui/material'
import { Box } from '@mui/system'


const Loader = ({loading = false}) => {

    return(
        <Box 
            display={loading ? "flex" : "none"}
            position="fixed"
            top={0}
            left={0}
            width="100%"
            height="100%"
            justifyContent="center"
            alignItems="center"
            bgcolor="rgba(0, 0, 0, 0.5)"
            zIndex="1600"
        >
            <Box>
                <CircularProgress style={{color: 'white'}}/>
            </Box>
        </Box>
    )
}

export default Loader;

