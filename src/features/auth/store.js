import { create } from 'zustand'
import { persist } from 'zustand/middleware'

export const useAuthStore = create(
  persist(
    (set, get) => ({
      token: null,
      logout: () => set({token: null}),
      authorized: ({token}) => set({token}),
    }),
    {
      name: 'auth-storage'
    }
  )
)

export const authActions = {
  logout: state => state.logout,
  authorized: state => state.authorized,
}

export const authSelector = {
  token: state => state.token,
  isLogged: state => state.token !== null,
}