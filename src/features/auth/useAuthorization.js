import { useEffect, useState } from "react";
import { authSelector, useAuthStore } from "./store";

export const useAuthorization = ({redirectTo, skip = false}) => {
    const [isAllowed, setIsAllowed] = useState(false);
    const isLogged = useAuthStore(authSelector.isLogged);

    useEffect(() => {
        if(skip || isLogged){
            setIsAllowed(true);
        }else{
            setIsAllowed(false);
            window.location.replace(redirectTo);
        }
    }, [skip, isLogged]);

    return {
        isAllowed
    }
}