import React, { createContext, Fragment, useCallback, useState } from 'react'
import { Box, Button, Modal, Stack } from '@mui/material';


const ModalContext = createContext({
    openModal: () => {},
    closeModal: () => {},
});

const initialState = {
    content: null,        
    preventClose: false,
}

export const ModalProvider = ({children}) => {
    const [modalOptions, setModalptions] = useState(initialState);
    const {content, preventClose} = modalOptions;

    const [open, setOpen] = useState(false);

    const openModal = useCallback((options) => {
        setOpen(true);
        setModalptions({...initialState, ...options});
    }, []);

    const closeModal = useCallback((event, reason) => {
        if(preventClose && (reason === "backdropClick" || reason === "escapeKeyDown"))
            return
        setOpen(false);
        setModalptions(initialState);
    }, [preventClose]);

    return (
        <ModalContext.Provider value={{openModal, closeModal}}>

            {children}

            <Modal open={open} onClose={closeModal}>
                <Box sx={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    transform: 'translate(-50%, -50%)',
                    bgcolor: 'white',
                    borderRadius: 3,
                    outline: 'none',
                    overflow: 'hidden'
                }}>
                    {content}                    
                </Box>
            </Modal>

        </ModalContext.Provider>
    )
}

export const useModal = () => {
    const { openModal, closeModal } = React.useContext(
        ModalContext
    );

    return {
        open: openModal,
        close: closeModal,
    };
};

export const useConfirmModal = () => {
    const { openModal, closeModal } = React.useContext(
        ModalContext
    );

    const open = useCallback((options) => {
        
        const { 
            content,
            onConfirm = () => {},
            confirmText = "Confirm",
            cancelText = "Cancel",
            ...other
        } = options
        
        const confirmContent = (
            <Box>
                {content}
                <Stack sx={{mt: 3, px: 2, pb: 2, justifyContent: 'flex-end'}} direction="row">
                    <Box>
                        <Button onClick={closeModal} variant="outlined">
                            {cancelText}
                        </Button>
                    </Box>
                    <Box ml={1}>
                        <Button sx={{px: 3}} onClick={onConfirm} variant="contained" disableElevation>
                            {confirmText}
                        </Button>
                    </Box>
                </Stack>
            </Box>
        )
        
        openModal({content: confirmContent, ...other})

    }, [])

    return {
        open,
        close: closeModal,
    };
};
  
  