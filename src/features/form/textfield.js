import { TextField as MuiTextfield, useControlled } from "@mui/material"
import { useController } from "react-hook-form"


export const TextField = ({fieldTable, ...rest}) => {
    
    const {
        field,
    } = useController(fieldTable)

    // console.log(fieldTable)

    return (
        <MuiTextfield 
            onChange={field.onChange}
            onBlur={field.onBlur}
            value={field.value}
            name={field.name}
            inputRef={field.ref}
            {...rest}
        />
    )
}