import { DatePicker as MuiDatePicker} from "@mui/x-date-pickers"
import { useController } from "react-hook-form"


export const Datepicker = ({formTable, ...rest}) => {
    
    const {
        field,
    } = useController(formTable)

    return (
        <MuiDatePicker 
            // onChange={field.onChange}
            onChange={(e) => { field.onChange(e.format('DD-MM-YYYY')) }}
            name={field.name}
            {...rest}
        />
    )
}