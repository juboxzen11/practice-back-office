import { useMemo, useCallback, useState, useEffect, Fragment } from "react";
import { Box, IconButton, Stack } from "@mui/material";
import { FaCheckCircle, FaPen, FaTimesCircle, FaTrashAlt } from "react-icons/fa";
import useDeepCompareEffect from "use-deep-compare-effect";
import { EditableInputs } from "./components/Editable";
import { useForm } from "react-hook-form";

export const useEditable = (props = {}) => {
    const {
        control = {},
        columns: baseColumns,
        data: baseData,
        onRowAdd,
        onRowUpdate,
        onRowDelete,
    } = props;

    const { state, table } = control;
    const { control: controlForm, reset: resetForm, handleSubmit } = useForm({mode: 'onChange'});
    const [editingIndex, setStateEditingIndex] = useState(null);
    const isEditing = useMemo(() => editingIndex !== null, [editingIndex]);
    const setEditingIndex = useCallback((index) => {
        resetForm()
        setStateEditingIndex(index)
    }, [])

    const isAddingRow = useMemo(() => editingIndex == -1, [editingIndex])
    const addRow = useCallback(() => {
        setEditingIndex(-1)
    }, []);
    const editRow = useCallback((info) => {
        setEditingIndex(info.row.index)
    }, []);
    const reset = useCallback(() => {
        setEditingIndex(null)
    }, []);

    //event actions
    const onConfrim = useCallback((info) => async (newData) => {
        let completed = false;

        if(isAddingRow){
            completed = onRowAdd ? await onRowAdd(newData) : true;
        }else{
            const { original: oldData } = info.row
            completed = onRowUpdate ? await onRowUpdate(newData, oldData) : true;
        }

        if(completed)
            reset()

    }, [onRowAdd, onRowUpdate, isAddingRow])

    const onCancel = useCallback(() => {
        reset()
    }, [])
    
    //modify columns
    const createColumns = useCallback((columns) => {
        const temp = [...columns({editRow, isEditing, isAddingRow, editingIndex})]

        const actionIndex = temp.findIndex(c => c.id == "editable")
        if(actionIndex != -1){
            const { cell } = temp[actionIndex];
            temp[actionIndex].cell = (info) => editingIndex === info.row.index ? 
                <Stack direction="row" justifyContent="center">
                    <IconButton color="primary" onClick={handleSubmit(onConfrim(info))}>
                        <FaCheckCircle size={25} />
                    </IconButton>
                    <IconButton color="primary" onClick={() => onCancel()}>
                        <FaTimesCircle size={25} />
                    </IconButton>
                </Stack>
            :
                cell(info)
        }

        //set editing
        temp = temp.map((value, columnIndex) => {
            const { cell = null } = value;
            
            const editableCell = (info) => {
                const renderCell = () => cell ? cell(info) : info.getValue()
                const {columnDef:{editable: {skip} = {}}} = info.column;
                
                if(isEditing){
                    const thisIsRowEditing = editingIndex === info.row.index;

                    if(thisIsRowEditing && !skip && columnIndex !== actionIndex)
                        return <EditableInputs info={info} control={controlForm} />

                    if(thisIsRowEditing && skip && isAddingRow)
                        return <Fragment />
                    
                    if(!thisIsRowEditing)
                        return <Box sx={{opacity: 0.5, pointerEvents: 'none'}}>{renderCell()}</Box>
                }

                return <Box>{renderCell()}</Box>
            }

            return {...value, cell: editableCell}
        })

        return temp;
    }, [editingIndex, isEditing, isAddingRow, editRow, onConfrim])

    const columns = useMemo(() => createColumns(baseColumns), [baseColumns, createColumns]);

    useDeepCompareEffect(() => reset(), [state])

    return {
        addRow,
        editRow,
        reset,
        isAddingRow,
        isEditing,
        columns
    }
}