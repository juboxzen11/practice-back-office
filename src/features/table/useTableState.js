import { useEffect, useMemo, useRef, useState } from "react";

export const useTableState = (props = {}) => {
    const {
        defaultState = null,
        defaultPageIndex = 0,
        defaultPageSize = 10,
        defaultSorting = [],
        defaultFilters = [],
    } = props;
    console.log('props = ',props)

    const initialState = useMemo(() => ({
        pagination: {
            pageIndex: defaultState?.pagination?.pageIndex || defaultPageIndex,
            pageSize: defaultState?.pagination?.pageSize || defaultPageSize,
        },
        sorting: defaultState?.sorting || defaultSorting,
        columnFilters: defaultState?.columnFilters || defaultFilters
    }), [props])
    
    const [state, setState] = useState(initialState)
    const [table, setTable] = useState(null)
    const containerRef = useRef(null)

    return { 
        initialState,
        state,
        setState,
        containerRef,
        control: {
            table,
            setTable,
            state,
            initialState,
            setState,
            containerRef
        }
    };
}