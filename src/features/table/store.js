import moment from 'moment'
import { useCallback, useEffect, useMemo } from 'react'
import { create } from 'zustand'
import { persist } from 'zustand/middleware'

const useStore = create(
    persist(
        (set) => ({
            tableData: {},
            storeTableData: (tableName, data) => {
                set(state => {
                    state.tableData[tableName] = data
                    return state
                })
            }
        }),
        {
            name: 'table-storage'
        }
    )
)

const ExpriedTime = 60 * 30
export const useTableStore = (tableName) => {
    const { tableData, storeTableData } = useStore()  // คือ create จาก zustand เป็น Function 

    // console.log('tableData =', tableData)

    // console.log('tableNameฝ =', tableName)

    const storeData = useMemo(() => {  
        const { data, expried } = tableData[tableName] || {}
        console.log('data = ',data ,
                    'expired = ',expried ,
                    'tableData = ',tableData
        )
        if (expried && moment().diff(moment(expried), 'seconds') > ExpriedTime)
            return undefined

        return data
    }, [])

    const updateStoreData = useCallback((data) => {
        storeTableData(tableName, { data, expried: moment().format() }) 
        // console.log(data)
    }, [tableName])

    const clearStoreData = useCallback((e) => {
        storeTableData(tableName, undefined)
    }, [tableName])

    return {
        storeData,
        updateStoreData,
        clearStoreData
    }
}