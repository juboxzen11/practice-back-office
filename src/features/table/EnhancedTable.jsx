import React, { useMemo, useRef, useEffect, useContext, createContext, Fragment } from 'react'
import { useReactTable, getCoreRowModel, flexRender, getPaginationRowModel, getSortedRowModel, getFilteredRowModel } from '@tanstack/react-table'
import { Box, Table, TableBody, TableCell, TableHead, TableRow, TableSortLabel, TableContainer } from '@mui/material'
import { PageSizeList } from './components/PageSizeList'
import { Pagination } from './components/Pagination'
import { styled } from '@mui/system';
import { useVirtual } from 'react-virtual'
import { EditableAddRow } from './components/Editable'
import numeral from 'numeral'
import { useCallback, useState } from 'react'

const TableStyled = styled('div')`
    .MuiTableContainer-root {
        border-radius: 5px;
    }
    table {
        thead {
            .MuiTableCell-root {
                padding: 0.75rem 1rem;
            }
            th.MuiTableCell-head {
                background-color: ${props => props.theme.palette.primary.main};
                color: white !important;
                font-weight: 600;
                white-space: nowrap;

                .MuiTableSortLabel-root
                    color: white !important;
                }
                .MuiTableSortLabel-icon {
                    color: white !important;
                }
            }
        }
        tbody {
            .MuiTableCell-root {
                padding: 0.5rem 1rem;
            }
            td {
                background-color: #f9f9f9;
            }
            tr:hover{
                td{
                    background-color: #f0f0f0;
                }
            }
        }
    }
`

export const EnhancedTableContext = createContext({
    parentRef: null
})

export const EnhancedTable = ({
    control = null,
    data = [],
    columns = [],
    totalRecord,
    loading = false,
    manualData = false,
    initialState,
    onStateChange,
    editable = null,
    pageLengths,
    pagination = true,
}) => {

    //resolve props
    initialState = useMemo(() => control?.initialState || initialState, [control]);
    onStateChange = useMemo(() => control?.setState || onStateChange, [control]);

    const table = useReactTable({    // ส่วนของ useReactTable ทั้งหมด
        data: data,
        columns: editable?.columns || columns,
        initialState,
        onStateChange,
        manualPagination: manualData,
        manualSorting: manualData,
        manualFiltering: manualData,
        getCoreRowModel: getCoreRowModel(),
        getSortedRowModel: getSortedRowModel(),
        getPaginationRowModel: pagination ? getPaginationRowModel() : undefined,
        getFilteredRowModel: getFilteredRowModel(),
        //debugTable: true,
    })


    const state = table.getState();
    const headerGroups = table.getHeaderGroups(); 
    const { rows } = table.getRowModel()

    console.log('state',state)

    //pagiation
    const [totalRecordState, setTotalRecordState] = useState(0);
    useEffect(() => {
        setTotalRecordState(totalRecord || data.length)
    }, [totalRecord, data.length])

    const { pageSize, pageIndex } = state.pagination;

    const pageCount = useMemo(() => Math.ceil(totalRecordState / state.pagination.pageSize), [totalRecordState, pageSize]);

    // useEffect(() => {
    //     table.setPageSize(-1)
    // }, [])

    //refs
    const { parentRef } = useContext(EnhancedTableContext)

    //virtualized rows
    const rowVirtualizer = useVirtual({
        parentRef: parentRef || { current: null },
        size: parentRef ? rows.length : 0,
        overscan: 10,
    })
    const { virtualItems: virtualRows, totalSize } = rowVirtualizer

    const paddingTop = virtualRows.length > 0 ? virtualRows?.[0]?.start || 0 : 0
    const paddingBottom =
        virtualRows.length > 0
            ? totalSize - (virtualRows?.[virtualRows.length - 1]?.end || 0)
            : 0

    const visibleLeafColumns = table.getVisibleLeafColumns()

    const rowRender = useCallback((row) => (
        <TableRow key={row.id}>
            {row.getVisibleCells().map((cell) => {
                const { align, sx = {} } = cell.column.columnDef;
                return (
                    <TableCell
                        key={cell.id}
                        align={align}
                        sx={sx}
                    // sx={{color: 'red',backgroundColor: 'red',p:10}}
                    >
                        {
                            flexRender(cell.column.columnDef.cell, cell.getContext())
                        }
                    </TableCell>
                )
            })}
        </TableRow>
    ), [])

    return (
        <TableStyled>
            {/* Pagination */}
            {pagination &&
                <Box sx={{ display: 'flex', flexWrap: 'wrap' }}>
                    <Box sx={{ mb: 1, mr: 1 }}>
                        <Box sx={{ display: 'flex', alignItems: 'center' }}>
                            <PageSizeList setPageSize={table.setPageSize} pageSize={pageSize} pageLengths={pageLengths} />
                        </Box>
                    </Box>
                    <Box sx={{ mb: 1, flex: { xs: 'auto', md: 1 }, display: 'flex', justifyContent: 'flex-end' }}>
                        <Pagination gotoPage={table.setPageIndex} pageCount={pageCount} pageIndex={pageIndex} loading={loading} />
                    </Box>
                </Box>
            }

            {/* Table */}
            <TableContainer>
                <Table stickyHeader>
                    <TableHead>
                        {headerGroups.map((headerGroup) => {
                            return (
                                <TableRow key={headerGroup.id}>
                                    {headerGroup.headers.map((header) => {
                                        return (
                                            <TableCell
                                                key={header.id}
                                                align={header.column.columnDef.align}
                                                colSpan={header.colSpan}
                                                sx={{
                                                    width: header.column.columnDef.width,
                                                    minWidth: header.column.columnDef.minWidth,
                                                    maxWidth: header.column.columnDef.maxWidth,
                                                }}
                                            >
                                                {header.isPlaceholder ? null : (
                                                    header.column.columnDef.enableSorting === false ?
                                                        <Box sx={{ mt: '1px' }}>
                                                            {flexRender(header.column.columnDef.header, header.getContext())}
                                                        </Box>
                                                        :
                                                        <TableSortLabel
                                                            active={header.column.getIsSorted() ? true : false}
                                                            direction={header.column.getIsSorted() || undefined}
                                                            onClick={header.column.getToggleSortingHandler()}
                                                        >
                                                            <Box sx={{
                                                                ml: header.column.columnDef.align === 'center' ? '24px' : 0 // offset for sort-arrow
                                                            }}>
                                                                {flexRender(header.column.columnDef.header, header.getContext())}
                                                            </Box>
                                                        </TableSortLabel>
                                                )}
                                            </TableCell>
                                        )
                                    })}
                                </TableRow>
                            )
                        })}
                    </TableHead>
                    <TableBody>

                        {/* Editable Render Add Row */}
                        {editable?.isAddingRow &&
                            <EditableAddRow columns={visibleLeafColumns} />
                        }

                        {paddingTop > 0 && (
                            <TableRow>
                                <TableCell colSpan={visibleLeafColumns.length} style={{ height: `${paddingTop}px` }} />
                            </TableRow>
                        )}

                        {
                            parentRef
                                ? virtualRows.map((virtualRow) => rowRender(rows[virtualRow.index]))
                                : rows.map(row => rowRender(row))
                        }
                        {paddingBottom > 0 && (
                            <TableRow>
                                <TableCell colSpan={visibleLeafColumns.length} style={{ height: `${paddingBottom}px` }} />
                            </TableRow>
                        )}

                    </TableBody>
                </Table>
            </TableContainer>

            {/* Record Info */}
            <Box sx={{ backgroundColor: '#f9f9f9', px: 2, py: 1, borderRadius: "0 0 5px 5px" }}>
                {loading ?
                    // show a loading indicator
                    <Box sx={{ color: 'primary.main', fontSize: '0.9rem', fontWeight: 500 }}>
                        Loading...
                    </Box>
                    :
                    rows.length == 0 ?
                        <Box sx={{ color: 'primary.main', fontSize: '0.9rem', fontWeight: 500, textAlign: 'center' }}>
                            No data available in table
                        </Box>
                        :
                        <Fragment>
                            {pagination &&
                                <Box sx={{ color: 'primary.main', fontSize: '0.9rem', fontWeight: 500 }}>
                                    Showing {numeral(pageIndex * pageSize + 1).format()} to {numeral(Math.min((pageIndex + 1) * pageSize, totalRecord)).format()} of {numeral(totalRecord).format()} results
                                </Box>
                            }
                        </Fragment>
                }
            </Box>

        </TableStyled>
    )
}
