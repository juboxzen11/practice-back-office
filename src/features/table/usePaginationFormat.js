import { useMemo } from "react"

export const usePaginationParser = (state) => {
    
    const pagination = useMemo(() => {
        const {pagination: {pageIndex, pageSize}, sorting} = state;
        
        return {
            page: pageIndex + 1
            , size: pageSize
            , orderBy: sorting[0]?.id
            , direction: sorting[0] ? (sorting[0].desc ? "desc" : "asc") : undefined
        } 
    }, [state])

    return pagination
}