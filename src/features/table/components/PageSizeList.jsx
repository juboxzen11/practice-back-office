import React, { Fragment } from 'react'
import { Button, Grow, MenuItem, MenuList, Paper, Popper, ClickAwayListener, Typography } from "@mui/material"
import numeral from 'numeral';

export const PageSizeList = ({
    pageSize,
    setPageSize,
    pageLengths = [
        {text: "10 Items", value: 10},
        {text: "20 Items", value: 20},
        {text: "50 Items", value: 50},
        {text: "100 Items", value: 100},
    ]
}) => {

    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);
  
    const handleToggle = () => {
      setOpen((prevOpen) => !prevOpen);
    };
  
    const handleClose = (event) => {
      if (
        anchorRef.current &&
        anchorRef.current.contains(event.target)
      ) {
        return;
      }
  
      setOpen(false);
    };
  
    function handleListKeyDown(event) {
      if (event.key === 'Tab') {
        event.preventDefault();
        setOpen(false);
      } else if (event.key === 'Escape') {
        setOpen(false);
      }
    }
  
    // return focus to the button when we transitioned from !open -> open
    const prevOpen = React.useRef(open);
    React.useEffect(() => {
      if (prevOpen.current === true && open === false) {
        anchorRef.current.focus();
      }
  
      prevOpen.current = open;
    }, [open]);

    return (
        <Fragment>
                <Button
                    ref={anchorRef}
                    aria-controls={open ? 'composition-menu' : undefined}
                    aria-expanded={open ? 'true' : undefined}
                    aria-haspopup="true"
                    onClick={handleToggle}
                    variant="outlined"
                    sx={{
                        boxShadow: 'none'
                    }}
                    color="secondary"
                >
                    <Typography sx={{textTransform: 'none', fontSize: '0.9rem'}}>
                        {numeral(pageSize).format()} Items per page
                    </Typography>
                </Button>
                <Popper
                    open={open}
                    anchorEl={anchorRef.current}
                    role={undefined}
                    placement="bottom-start"
                    transition
                    disablePortal
                    style={{zIndex: 10}}
                >
                    {({ TransitionProps, placement }) => (
                    <Grow
                        {...TransitionProps}
                        style={{
                            transformOrigin: placement === 'bottom-end' ? 'right top' : 'right bottom',
                        }}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={handleClose}>
                                <MenuList
                                autoFocusItem={open}
                                id="composition-menu"
                                aria-labelledby="composition-button"
                                onKeyDown={handleListKeyDown}
                                sx={{my: 1}}
                                >
                                    {pageLengths.map(item =>
                                        <MenuItem key={item.value} 
                                            onClick={(e) => {
                                                handleClose(e);
                                                setPageSize(item.value)
                                            }}
                                        >
                                            {item.text}
                                        </MenuItem>
                                    )}
                                </MenuList>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                    )}
                </Popper>
        </Fragment>
    )
}