
import React, { useCallback, useEffect } from 'react'
import { Pagination as MuiPagination } from '@mui/material'
import { styled } from '@mui/system';

const PaginationStyles = styled('div')`
    .MuiPaginationItem-root{
        color: #888;
        background-color: white;
        &.MuiPaginationItem-previousNext{
            color: black;
        }
    }
    .MuiPaginationItem-root.Mui-selected{
        color: black;
        background-color: #eee;
        font-weight: 600;
    }
`

export const Pagination = ({
    pageIndex,
    pageCount,
    gotoPage,
    loading
}) => {

    const onChange = useCallback((event, value) =>{
        gotoPage(value - 1);
    }, [gotoPage]);

    useEffect(() => {
        const timer = setTimeout(() => {
            if(!loading && pageIndex + 1 > pageCount)
                gotoPage(0)
        }, 100);
        return () => clearTimeout(timer);
    }, [pageIndex, pageCount, loading])

    return (
        <PaginationStyles>
            <MuiPagination 
                shape="rounded"
                count={pageCount}
                page={pageIndex + 1}
                onChange={onChange}
            />
        </PaginationStyles>
    )
}