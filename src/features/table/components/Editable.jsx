import React, { useCallback, useMemo } from "react"
import { TableCell, TableRow, TextField } from "@mui/material"
import { Controller } from "react-hook-form"

export const EditableAddRow = ({columns}) => {
    return (
        <TableRow>
            {columns.map((column) => {
                return (
                    <TableCell
                        key={column.id}
                        align={column.columnDef.align}
                    >
                        {column.columnDef.cell({column, row: {index: -1, original: {}}})}
                    </TableCell>
                )
            })}
        </TableRow>
    )
}

export const EditableInputs = ({info, control}) => {

    const {original} = info.row;
    const {id, columnDef: {
        editable: {
            rules, 
            render, 
            defaultValue: editableDefaultValue,
            skip
        } = {}
    }} = info.column;
    
    const defaultValue = useMemo(() =>
        editableDefaultValue !== undefined
            ? editableDefaultValue(info)
            : (original[id] !== undefined && original[id] !== null ? original[id] : undefined)
    , [info])
    
    const defaultRender = useCallback(({field: {ref, ...field}, fieldState: {error}}) => (
        <TextField
            {...field}
            value={field.value || ""}
            inputRef={ref}
            variant="standard" 
            fullWidth
            error={error ? true : false}
            helperText={error?.message}
        />
    ))
    
    return (
        <Controller
            control={control}
            name={id}
            defaultValue={defaultValue}
            rules={rules}
            render={render || defaultRender}
        />
    )
}
