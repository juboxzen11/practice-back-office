import { useMemo } from "react";
import { useMediaQuery, useTheme } from "@mui/material";
import { sidebarSelector, useSidebarStore } from "./store";

export const useSidebarMarginLeft = ({fullbarWidth, minibarWidth}) => {
    const theme = useTheme();
    const isMediumDown = useMediaQuery(theme.breakpoints.down('md'));
    const isFullBar = useSidebarStore(sidebarSelector.isFullBar);
    const sidebarMarginLeft = useMemo(() => {
        if(isMediumDown)
            return 0;
        return isFullBar ? fullbarWidth : minibarWidth;
    }, [isMediumDown, isFullBar, fullbarWidth, minibarWidth]);

    return sidebarMarginLeft
}