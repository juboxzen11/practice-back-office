import { create } from 'zustand'
import { persist } from 'zustand/middleware'
import { DISPLAY_STATE, SIZE_STATE } from './contants'

const initialState = {
    hover: false,
    barDisplayState: 'HIDE',
    barSizeState: 'FULL_BAR',
}

const createActions = (set) => ({
    showBar: () => {
        set({barDisplayState: DISPLAY_STATE.SHOW})
    },
    hideBar: () => {
        set({barDisplayState: DISPLAY_STATE.HIDE})
    },
    toggleShownBar: () => {
        set(state => {
            if(state.barDisplayState == DISPLAY_STATE.SHOW)
                state.barDisplayState = DISPLAY_STATE.HIDE;
            else
                state.barDisplayState = DISPLAY_STATE.SHOW;
              
            return {...state}
        })
       
    },
    toggleSizeBar: () => {
        set(state => {
            if(state.barSizeState == SIZE_STATE.FULL_BAR)
                state.barSizeState = SIZE_STATE.MINI_BAR;
            else
                state.barSizeState = SIZE_STATE.FULL_BAR;

            state.hover = false; //for reset hover
            return {...state}
        })
    },
    enterBar: () => {
        set({hover: true})
    },
    leaveBar: () => {
        set({hover: false})
    }
})

export const useSidebarStore = create(
  persist(
    (set) => ({
        ...initialState,
        ...createActions(set)
    }),
    {
      name: 'sidebar-storage'
    }
  )
)

export const sidebarActions = {
    showBar: state => state.showBar,
    hideBar: state => state.hideBar,
    toggleShownBar: state => state.toggleShownBar,
    toggleSizeBar: state => state.toggleSizeBar,
    enterBar: state => state.enterBar,
    leaveBar: state => state.leaveBar,
}

export const sidebarSelector = {
    isFullBar: state => state.barSizeState == SIZE_STATE.FULL_BAR,
    isMiniBar: state => state.barSizeState == SIZE_STATE.MINI_BAR,
    isShowBar: state => state.barDisplayState == DISPLAY_STATE.SHOW,
    isHideBar: state => state.barDisplayState == DISPLAY_STATE.HIDE,
    isHoverBar: state => state.hover,
}