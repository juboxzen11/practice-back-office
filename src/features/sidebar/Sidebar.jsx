import React, { Fragment, ReactElement, useCallback, useEffect, useMemo, useState } from "react";
import { Drawer, IconButton, Stack, useMediaQuery, useTheme } from "@mui/material";
import { Box } from "@mui/system"
import { AiOutlineMenu, AiOutlineMenuFold, AiOutlineMenuUnfold} from 'react-icons/ai';
import { sidebarActions, sidebarSelector, useSidebarStore } from "./store";

export const Sidebar = ({fullbarWidth, minibarWidth, children}) => {
    const theme = useTheme();
    const isMediumDown = useMediaQuery(theme.breakpoints.down('md'));
    const isMediumUp = !isMediumDown;
    const isShowBar = useSidebarStore(sidebarSelector.isShowBar);
    const isFullBar = useSidebarStore(sidebarSelector.isFullBar);
    const isMiniBar = useSidebarStore(sidebarSelector.isMiniBar);
    const isHoverBar = useSidebarStore(sidebarSelector.isHoverBar);
    const enterBar = useSidebarStore(sidebarActions.enterBar);
    const leaveBar = useSidebarStore(sidebarActions.leaveBar);
    const hideBar = useSidebarStore(sidebarActions.hideBar);

    const rootSideBarWidth = useMemo(() => {
        if(isMediumDown)
            return fullbarWidth;
        return isFullBar ? fullbarWidth : minibarWidth;
    }, [isFullBar, isMediumDown]);

    const bodySideBarWidth = useMemo(() => {
        if(isMediumDown)
            return fullbarWidth;
        return (isFullBar || isHoverBar) ? fullbarWidth : minibarWidth;
    }, [isFullBar, isHoverBar, isMediumDown]);

    //set hover state with timeout
    const [mouseEnter, setMouseEnter] = useState(false);
    useEffect(() => {
        let timout;
        //console.log('mouseEnter', mouseEnter);
        if(mouseEnter){
            timout = setTimeout(() => {
                enterBar()
            }, 150);
        }else{
            leaveBar()
        }
        return () => clearTimeout(timout)
    }, [mouseEnter]);

    return (
        <Drawer
            sx={{
                transition: `width 150ms ease-in-out`,
                width: rootSideBarWidth + 'px',
                '& .MuiDrawer-paper': { 
                    backgroundColor: 'transparent', boxShadow: 'none', border: 'none', overflow: 'visible'
                },
            }}
            variant={isMediumDown ? "temporary" : "permanent"}
            open={isShowBar}
            anchor="left"
            onMouseEnter={() => setMouseEnter(isMediumUp && true)}
            onMouseLeave={() => setMouseEnter(false)}
            onClose={() => hideBar()}
            ModalProps={{
                keepMounted: true, // Better open performance on mobile.
            }}
        >
            <Stack sx={{ 
                height: 1,
                transition: `width 150ms ease-in-out`,
                width: bodySideBarWidth + 'px',
                overflow: 'hidden', 
                bgcolor: 'background.default',
                boxShadow: '0px 2px 5px 2px rgba(0,0,0,0.1)'
            }}>
                {children}
            </Stack>
        </Drawer>
    )
}

export const OpenBarButton = () => {
    const theme = useTheme();
    const isMediumDown = useMediaQuery(theme.breakpoints.down('md'));
    const showBar = useSidebarStore(sidebarActions.showBar);

    const onClick = useCallback((e) => {
        if(isMediumDown)
            showBar()
    }, [isMediumDown]);

    return (
        <IconButton onClick={onClick}>
            <AiOutlineMenu size={28} color="white" />
        </IconButton>
    )
}

export const ToggleBarButton = () => {    
    const theme = useTheme();
    const isMediumUp = useMediaQuery(theme.breakpoints.up('md'));
    const isFullBar = useSidebarStore(sidebarSelector.isFullBar);
    const toggleSizeBar = useSidebarStore(sidebarActions.toggleSizeBar);
    const onClick = useCallback(() => {
        if(isMediumUp)
            toggleSizeBar()
    }, [isMediumUp]);

    return (
        <IconButton onClick={onClick}>
            {isFullBar ?
                <AiOutlineMenuFold size={28} color="white" />
            :
                <AiOutlineMenuUnfold size={28} color="white" />
            }
        </IconButton>
    )
}
