export const DISPLAY_STATE = {
    SHOW: 'SHOW',
    HIDE: 'HIDE'
}
export const SIZE_STATE = {
    FULL_BAR: "FULL_BAR",
    MINI_BAR: "MINI_BAR"
}