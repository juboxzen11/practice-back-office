import { create } from 'zustand'
import { persist } from 'zustand/middleware'

export const COLOR_MODE = {
  LIGHT: 'light',
  DARK: 'dark'
}

export const useDarkModeStore = create(
  persist(
    (set, get) => ({
      mode: 'light',
      toggleMode: () => set(state => ({mode: state.mode === 'light' ? 'dark' : 'light'}))
    }),
    {
      name: 'mode-storage'
    }
  )
)
