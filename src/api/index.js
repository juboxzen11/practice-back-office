import { useEffect } from 'react'
import axios from "axios";

export const axiosInstance = axios.create({
	baseURL: process.env.NEXT_PUBLIC_BASE_API_URL,
	headers: {
		post: { 'Content-Type': 'application/json' }
	}
});

export const useTokenAuthentication = ({accessToken = null}) => {
    
    useEffect(() => {
        const requestIntercept = axiosInstance.interceptors.request.use(
            config => {
                if (accessToken && !config.headers['Authorization'])
                    config.headers['Authorization'] = `Bearer ${accessToken}`;

                return config;
            }, (error) => Promise.reject(error)
        );

        const responseIntercept = axiosInstance.interceptors.response.use(
            response => response,
            async (error) => {

                //RefreshToken
                // const prevRequest = error?.config;
                // if (error?.response?.status === 403 && !prevRequest?._retry ) {
                //     prevRequest._retry  = true;
                //     await refresh();
                //     return axiosInstance(prevRequest);
                // }
            
                return Promise.reject(error);
            }
        );

        return () => {
            axiosInstance.interceptors.request.eject(requestIntercept);
            axiosInstance.interceptors.response.eject(responseIntercept);
        }
    }, [accessToken])

}