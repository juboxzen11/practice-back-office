import { axiosInstance } from "..";

export const MessageLogAPI = {
    Get : (data) => axiosInstance({
        url: 'api/MessageLog/Get',
        method: 'POST',
        data
    }).then(res => res.data),
}