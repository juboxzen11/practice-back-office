import { axiosInstance } from "..";

export const Account = {
    Login : (data) => axiosInstance({
        url: 'api/Account/Login',
        method: 'POST',
        data
    }),
    Me : (data) => axiosInstance({
        url: 'api/Account/Me',
        method: 'POST',
        data
    }).then(res => res.data),
}