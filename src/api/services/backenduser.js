import { axiosInstance } from "..";

export const BackendUserAPI = {
    Get : (data) => axiosInstance({
        url: 'api/BackendUser/Get',
        method: 'POST',
        data
    }).then(res => res.data),
}