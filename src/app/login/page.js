'use client'
import { Fragment, useState, useCallback } from "react";
import { useRouter } from "next/navigation";
import Loader from "@/features/loader/Loader";
import Image from "next/image";
import { Box, Button, IconButton, InputAdornment, Stack, TextField, useTheme } from "@mui/material";
import { FaEye, FaEyeSlash, FaLock, FaUserAlt } from "react-icons/fa";
import { useForm } from "react-hook-form";
import { grey } from "@mui/material/colors";
import { toast } from "react-toastify";
import { authActions, useAuthStore } from "@/features/auth/store";
import { useMutation } from "@tanstack/react-query";
import { Account } from "@/api/services/account";
import { HTTPSTATUS } from "@/api/constants";

export default function Login() {
  
  const router = useRouter();
  const [showPassword, setShowPassword] = useState(false);
  const { register, handleSubmit, formState: { errors } } = useForm();
  const authorized = useAuthStore(authActions.authorized);

  const { mutateAsync, isLoading } = useMutation({mutationFn: Account.Login})
  const onSubmit = useCallback(async (data) => {

    const { username, password } = data;
    const { data: response } = await mutateAsync({ username, password })

    if (response) {

      if (response?.status == HTTPSTATUS.UNAUTHORIZED) {
        toast.warn(`Invalid username or password.`);
        return;
      }

      if (response?.status != HTTPSTATUS.SUCCESS) {
        toast.warn(`[${response.status}] ${response.description}`);
        return;
      }

      const { token } = response.payload;
      authorized({ token });
      toast.success('Login Success.', { autoClose: 1500 });
      router.push('/');
    }

  }, []);

  return (
    <Fragment>

      <Loader loading={isLoading} />

      {/* Login Content */}
      <Stack sx={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        px: 3,
        height: '100vh',
        background: `linear-gradient(135deg, white, ${grey[500]})`
      }}>
        <Stack sx={{
          flexDirection: 'row',
          borderRadius: 3,
          boxShadow: 5,
          overflow: 'hidden'
        }}>
          {/* Undraw Panel */}
          <Stack sx={{
            display: { xs: 'none', md: 'flex' },
            width: 400,
            backgroundColor: grey[200],
            position: 'relative'
          }}>
            <Box sx={{
              position: 'absolute',
              bottom: 0,
              left: 0,
            }}>
              {/* <Image src="/images/login_image.svg"  width={800} height={500} alt="undraw" quality={100} priority/> */}
            </Box>
          </Stack>

          {/* Login Form Panel */}
          <form onSubmit={handleSubmit(onSubmit)}>
            <Stack sx={{
              alignItems: 'center',
              width: 1,
              minWidth: 300,
              maxWidth: 400,
              px: 3,
              py: 6,
              backgroundColor: 'white',
              boxShadow: 5,
              zIndex: 1,
            }}>

              <Box sx={{ fontSize: '1.75rem' }}>
                LOGIN
              </Box>

              <Box sx={{ fontWeight: 300, fontSize: '1rem' }}>
                WELCOME TO BACKOFFICE
              </Box>

              <Box sx={{ width: { xs: 250, sm: 300 } }}>

                {/* Line */}
                <Box sx={{ mt: 2, mb: 5, backgroundColor: 'grey.300', height: '1px', width: 1 }} />

                <Box>
                  <TextField
                    {...register('username', { required: true })}
                    helperText={errors.username && "This field is required."}
                    error={errors?.username ? true : false}
                    fullWidth
                    label="Username"
                    variant="filled"
                    size="small"
                    placeholder="Username"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <FaUserAlt />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Box>
                <Box sx={{ mb: 3 }} />
                <Box>
                  <TextField
                    {...register('password', { required: true })}
                    helperText={errors.password && "This field is required."}
                    error={errors?.password ? true : false}
                    fullWidth
                    type={showPassword ? "text" : "password"}
                    label="Password"
                    variant="filled"
                    size="small"
                    placeholder="●●●●●●●●"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <FaLock />
                        </InputAdornment>
                      ),
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={() => setShowPassword(prev => !prev)}
                            edge="end"
                          >
                            {!showPassword ? <FaEye /> : <FaEyeSlash />}
                          </IconButton>
                        </InputAdornment>
                      )
                    }}
                  />
                </Box>
                <Box sx={{ mb: 5 }} />

                <Button
                  type="submit"
                  sx={{ px: 5, py: 1 }}
                  variant="contained"
                  fullWidth
                  size="large"
                >
                  SUBMIT
                </Button>
              </Box>

              <Box sx={{ mb: 3 }} />
            </Stack>
          </form>
        </Stack>
      </Stack>

    </Fragment>
  )
}