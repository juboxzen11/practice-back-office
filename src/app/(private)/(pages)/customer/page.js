'use client'
import { BackendUserAPI } from "@/api/services/backenduser";
import { EnhancedTable } from "@/features/table/EnhancedTable";
import { useTableStore } from "@/features/table/store";
import { usePaginationParser } from "@/features/table/usePaginationFormat";
import { useTableState } from "@/features/table/useTableState";
import { Box, Stack, TextField } from "@mui/material";
import { useQuery, keepPreviousData } from "@tanstack/react-query";
import moment from 'moment';
import { useEffect, useMemo } from "react";
import { useForm } from "react-hook-form";

export default function Customer() {
    const { storeData, updateStoreData } = useTableStore('customer-list-table') // storeData 
    const { register, watch } = useForm({ defaultValues: storeData?.inputs });

    // const { register, watch } = useForm({ defaultValues: { firstName: 'get', lastName: 'abc' } });

    const inputs = watch();
    const { control, state } = useTableState({ defaultState: storeData?.tableState, defaultSorting: [{ id: "createdAt", desc: true }] })

    const testInfo = new Array(10)
    const testMap = testInfo.map((item,i) => {
    })
    console.log('testMap',testMap)

    const filters = useMemo(
        () => 
    ({ inputs, tableState: state }), [inputs, state] // tableState
    )

    // console.log('filters', filters)
    
    useEffect(() => updateStoreData(filters), [filters])

    // console.log('filters = ',filters,'[filters] = ',[filters])
    
    const pagination = usePaginationParser(filters.tableState) //  tableState  

    const { data: response, isFetching } = useQuery({  // data เปลี่ยนชื่อเป็น response
        queryKey: ['user-get', filters],  // queryKey มีส่วนในการช่วยในเรื่องของการ filter , search
        queryFn: () => BackendUserAPI.Get({ ...filters.inputs, ...pagination }),
        placeholderData: keepPreviousData

    })
    
    const data = useMemo(() => response?.payload.list || [], [response])
    const totalRecord = useMemo(() => response?.payload.totalRecord || 0, [response])


    const columns = useMemo(() => [
        {
            accessorKey: 'userName', // key - property ที่ต้องตรงกับ api ที่ส่งมา
            header: 'Username', // หัวข้อ  column
        },
        {
            accessorKey: 'firstName',
            header: 'First Name',
        },
        {
            accessorKey: 'lastName',
            header: 'Last Name',
        },
        {
            id: 'roleId',
            accessorFn: (e) => e.roleName || "-",
            header: 'Role Name',
        },
        {
            accessorKey: 'empCode',
            header: 'Empcode',
        },
        {
            accessorKey: 'supEmpCode',
            header: 'Super Empcode',
        },
        {
            accessorKey: 'unitCode',
            header: 'Unit Code',
        },
        {
            accessorKey: 'unitNameLvl4',
            header: 'Department By',
            // enableSorting: false
        },
        {
            accessorKey: 'createdAt',
            header: 'Create Date',
            cell: info => moment(info.getValue()).format("DD/MM/YYYY"),
            width: 200

        },
    ], [])

    return (
        <Box>
            <Stack sx={{ p: 2, flex: 1, alignItems: 'center' }} direction="row">
                <Box sx={{ fontSize: '1.5rem', fontWeight: 600 }}>
                    CUSTOMER
                </Box>
                <Box mx={1}>-</Box>
                <Box sx={{ fontSize: '1.25rem', fontWeight: 500 }}>
                    LIST
                </Box>
            </Stack>
            <Box sx={{ px: 2, pb: 2 }}>
                <Box sx={{ bgcolor: "white", boxShadow: 2 }}>
                    <Box sx={{ height: 5, width: 1, bgcolor: "primary.main" }} />

                    <Box sx={{ p: 2 }}>

                        {/* Filters */}
                        <Stack direction="row" flexWrap="wrap">
                            <Box mr={1} mb={1}>
                                <TextField
                                    {...register("firstName")}
                                    label='First Name'
                                    size="small"
                                />
                            </Box>
                            <Box mr={1} mb={1}>
                                <TextField
                                    {...register("lastName")}
                                    label='Last Name'
                                    size="small"
                                />
                            </Box>
                            <Box mr={1} mb={1}>
                                <TextField
                                    {...register("empCode")}
                                    label='Empcode'
                                    size="small"
                                />
                            </Box>
                            <Box mr={1} mb={1}>
                                <TextField
                                    {...register("unitcode")}
                                    label='Unit Code'
                                    size="small"
                                />
                            </Box>
                        </Stack>
                       
                        <Box sx={{ mt: 0.5, mb: 1.5, bgcolor: "grey.300", height: '1px' }} />
                        <EnhancedTable
                            control={control} // มีส่วนเกี่ยวข้องกับการ sorting
                            columns={columns} // 
                            data={data}
                            totalRecord={totalRecord}
                            manualData={true}
                            loading={isFetching}
                        />
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}