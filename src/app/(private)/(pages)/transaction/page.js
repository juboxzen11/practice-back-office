'use client'
import { Box, Button, Stack } from "@mui/material";
import { useForm, useWatch } from "react-hook-form";
import { useEffect, useMemo, useRef, useState } from "react";
import { EnhancedTable } from "@/features/table/EnhancedTable";
import { BackendUserAPI } from "@/api/services/backenduser";
import { useQuery, keepPreviousData } from "@tanstack/react-query";
import { useTableStore } from "@/features/table/store";
import { useTableState } from "@/features/table/useTableState";
import moment from 'moment';
import {
    flexRender, // จะช่วยเรื่องของการ รับ accessorFn
    // เปลี่ยนสไตล์ต่างๆ ที่รับจากค่าจาก Column - cells
    // เพื่อใช้เปลี่ยนค่า หรือ format ต่างๆ 
    getCoreRowModel,
    getPaginationRowModel,
    getSortedRowModel,
    useReactTable,
} from "@tanstack/react-table";
import { TextField } from "@/features/form/textfield";
import { Datepicker } from "@/features/form/datepicker";
import { useVirtualizer } from '@tanstack/react-virtual';
import { DatePicker } from "@mui/x-date-pickers";

export default function Transaction() {

    const { reset, control: formTable } = useForm()

    const [sorting, setSorting] = useState([]); // คือ Function เอาไว้ sorting ร่วมกับ ReacTable

    // const inputs = watch();

    const inputs = useWatch({ control: formTable });

    // console.log('input-useWatch = ', inputs)


    // const data = useMemo(() => [
    //     { userName: 'aaaaa', firstName: 'dddddd', lastName: 'cccccc', dateTime: '10/10/2023' },
    //     { userName: 'aa4343aa', firstName: 'dddddd', lastName: 'cccccc', dateTime: '12/11/2023' },
    //     { userName: 'bb', firstName: 'dddddd', lastName: 'cccccc', dateTime: '09/10/2023' },
    //     { userName: 'aaaaa', firstName: 'dddddd', lastName: 'cccccc', dateTime: '12/08/2023' },
    // ], []
    // )
    const data1 = useMemo(() => {
        return new Array(30)
    }, [])
    const data2 = useMemo(() => {
        for (let i = 0; i < data1.length; i++) {
            if (i >= 0 && i <= 9) { data1[i] = { index: i, userName: '1111' } }
            if (i >= 10 && i <= 19) { data1[i] = { index: i, userName: '22222' } }
            if (i >= 20 && i <= 29) { data1[i] = { index: i, userName: '333333' } }
        }
    }, [])

    console.log('data1 from ', data1)

    const columns = useMemo(() => [
        {
            accessorKey: 'index', // key - property ที่ต้องตรงกับ api ที่ส่งมา
            header: 'Index', // หัวข้อ  column
        },
        {
            accessorKey: 'userName', // key - property ที่ต้องตรงกับ api ที่ส่งมา
            header: 'User name', // หัวข้อ  column
        },
        {
            accessorKey: 'firstName',
            header: 'First Name',
        },
        {
            accessorKey: 'lastName',
            header: 'Last Name',
        },
        {
            accessorKey: 'dateTime',
            header: 'DateTime',
            cell: info => moment(info.getValue()).format("MM/DD/YYYY")
            // cell: info => console.log('info in Column-Header = ',info) 
        },
    ], []
    );

    const table = useReactTable({
        data: data1,
        columns,
        state: {
            sorting,
        },
        onSortingChange: setSorting,
        getSortedRowModel: getSortedRowModel(),
        getCoreRowModel: getCoreRowModel(),
        // getPaginationRowModel: getPaginationRowModel(),
        // debugTable: true,
    });
    // console.log('table = ', table)


    // The scrollable element for your list
    const parentRef = useRef()


    // The virtualizer
    const rowVirtualizer = useVirtualizer({
        count: 30,
        getScrollElement: () => parentRef.current,
        estimateSize: () => 30,
    })

    // console.log(data1)
    // console.log('test1 = ', rowVirtualizer)
    // console.log(' test2 = ',rowVirtualizer.getVirtualItems())

    // const testInfo = rowVirtualizer.getVirtualItems().map(item => item)


    return (
        <Box>
            <Box sx={{ bgcolor: "white", boxShadow: 2, m: 1 }}>
                <Box sx={{ height: 5, width: 1, bgcolor: "primary.main" }} />
                <Box sx={{ p: 2 }}>
                    {/* Filters */}
                    <Stack direction="row" flexWrap="wrap">
                        <Box mr={1} mb={1}>
                            <TextField
                                fieldTable={{
                                    control: formTable,
                                    name: 'userName',
                                    defaultValue: ''
                                }}
                                label='User Name'
                                size="small"
                            />
                        </Box>
                        <Box mr={1} mb={1}>
                            <TextField
                                fieldTable={{
                                    control: formTable,
                                    name: 'firstName',
                                    defaultValue: ''
                                }}
                                label='First Name'
                                size="small"
                            />
                        </Box>
                        <Box mr={1} mb={1}>
                            <TextField
                                fieldTable={{
                                    control: formTable,
                                    name: 'lastName',
                                    defaultValue: ''
                                }}
                                label='last Name'
                                size="small"
                            />
                        </Box>
                    </Stack>
                    <Stack flexWrap="wrap" gap='10px' alignItems='center' sx={{ flexDirection: 'row', my: 2 }}>
                        <Datepicker
                            formTable={{
                                control: formTable,
                                name: "startDate"
                            }}
                            label='start'
                        />
                        -
                        <Datepicker
                            formTable={{
                                control: formTable,
                                name: "endDate"
                            }}
                            label='End'
                        />
                        <DatePicker label='End' />
                        <Box sx={{ mt: 0.5, mb: 1.5, bgcolor: "grey.300", height: '1px' }} />
                        <Button onClick={() => reset()}>Clear</Button>
                    </Stack>
                    <Box ref={parentRef} sx={{ height: '350px', overflow: 'auto', border: '1px solid black', width: '100%', display: 'flex' }}>
                        <Box sx={{ height: `${rowVirtualizer.getTotalSize()}px` }}>
                            <table>
                                <thead>
                                    {table.getHeaderGroups().map(headerGroup => (
                                        <tr key={headerGroup.id}>
                                            {headerGroup.headers.map(header => {
                                                // console.log(table.getRowModel())
                                                // console.log('table = ',table.getVisibleCells()) 
                                                // console.log('table-getRowModel = ', table.getRowModel())
                                                // console.log('Context = ', header.getContext())
                                                // console.log('header = ', header)
                                                // console.log('Header = ', header.column.columnDef.header)
                                                // console.log('HeaderGetCanSort = ', header.column.getCanSort())
                                                // console.log("header.isPlaceholder = ", header.isPlaceholder)
                                                // console.log('header.getIsSorted = ', header.column.getIsSorted())
                                                // console.log('sorting', sorting)

                                                return (
                                                    <th
                                                        onClick={header.column.getToggleSortingHandler()}
                                                        style={{ cursor: 'pointer', padding: '10px' }} key={header.id}>

                                                        {flexRender(
                                                            header.column.columnDef.header,
                                                            header.getContext()
                                                        )
                                                        }
                                                        <span>
                                                            {header.column.getIsSorted() ?
                                                                (header.column.getIsSorted() == 'desc' ? '🔼' : '🔽') : ''}
                                                        </span>
                                                    </th>
                                                )
                                            })}
                                        </tr>
                                    ))}
                                </thead>
                                <tbody>
                                    {table
                                        .getRowModel()
                                        .rows.map(row => {
                                            return (
                                                <tr key={row.id}>
                                                    {row.getVisibleCells().map(cell => {
                                                        return (
                                                            <td key={cell.id}>
                                                                {flexRender(
                                                                    cell.column.columnDef.cell,
                                                                    cell.getContext()
                                                                )}
                                                            </td>
                                                        )
                                                    })}
                                                </tr>
                                            )
                                        })}
                                </tbody>
                            </table>
                        </Box>
                        {/* <Box sx={{ marginLeft: 4 }}>
                            <button
                                className="border rounded p-1"
                                onClick={() => table.setPageIndex(0)}
                            // disabled={!table.getCanPreviousPage()}
                            >
                                {'<<'}
                            </button>
                            <button
                                className="border rounded p-1"
                                onClick={() => table.previousPage()}
                            // disabled={!table.getCanPreviousPage()}
                            >
                                {'<'}
                            </button>
                            <button
                                className="border rounded p-1"
                                onClick={() => table.nextPage()}
                            // disabled={!table.getCanNextPage()}
                            >
                                {'>'}
                            </button>
                            <button
                                className="border rounded p-1"
                                onClick={() => table.setPageIndex(table.getPageCount() - 1)}
                            // disabled={!table.getCanNextPage()}
                            >
                                {'>>'}
                            </button>
                            <select
                                value={table.getState().pagination.pageSize}
                                onChange={e => {
                                    table.setPageSize((e.target.value))
                                }}
                            >
                                {[5, 20, 30, 40, 50].map(pageSize => (
                                    <option key={pageSize} value={pageSize}>
                                        Show {pageSize}
                                    </option>
                                ))}
                            </select>
                        </Box> */}
                    </Box>
                    {/* /////////////////// Example for Virtualizer /////////////////// */}
                    <>
                        {/* The scrollable element for your list */}
                        <div
                            // ref={parentRef}
                            style={{
                                marginTop: '20px',
                                height: '350px',
                                width: '100px',
                                overflow: 'auto', // Make it scroll!
                            }}
                        >
                            {/* The large inner element to hold all of the items */}
                            <div
                                style={{
                                    height: `${rowVirtualizer.getTotalSize()}px`,
                                    width: '100%',
                                    position: 'relative',
                                }}
                            >
                                {/* Only the visible items in the virtualizer, manually positioned to be in view */}
                                {/* {rowVirtualizer.getVirtualItems().map((virtualItem) => (
                                    <div
                                        key={virtualItem.key}
                                        style={{
                                            position: 'absolute',
                                            top: 0,
                                            left: 0,
                                            width: '100%',
                                            height: `${virtualItem.size}px`, // กำหนด Height ใหม่
                                            // ไปพร้อมกับ ค่าใหม่ของ transform
                                            transform: `translateY(${virtualItem.start}px)`,
                                        }}
                                    >
                                        Row {virtualItem.index}
                                    </div>
                                ))} */}
                            </div>
                        </div>
                    </>
                    {/* /////////////////// Example for Virtualizer /////////////////// */}
                    {/* <EnhancedTable
                        control={tableControl} // มีส่วนเกี่ยวข้องกับการ sorting
                        columns={columns}
                        data={data}
                        totalRecord={totalRecord}
                        manualData={true}
                        // loading={isFetching}
                        changeStyle={true}
                    /> */}
                    <Box >
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}