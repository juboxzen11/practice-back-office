import { authActions, useAuthStore } from "@/features/auth/store";
import { OpenBarButton, ToggleBarButton } from "@/features/sidebar/Sidebar";
import { Divider, IconButton, ListItem, Popover, Stack, useMediaQuery, useTheme } from "@mui/material";
import { Box } from "@mui/system";
import React, { Fragment, useCallback, useContext } from "react";
import { FaUserCircle } from "react-icons/fa";
import { MdDarkMode, MdLightMode, MdLogout } from "react-icons/md";
import { MainLayoutContext } from "./layout";
import { useDarkModeStore } from "@/features/dark-mode/store";

export const TopbarBody = () => {
    const theme = useTheme();
    const isMediumDown = useMediaQuery(theme.breakpoints.down('md'));

    // topbar menu
    const [anchorEl, setAnchorEl] = React.useState(null);
    const openMenu = useCallback((e) => {
        setAnchorEl(e.currentTarget);
    }, []);
    const closeMenu = useCallback((e) => {
        setAnchorEl(null);
    }, []);

    const logout = useAuthStore(authActions.logout);
    const onLogout = useCallback(() => {
        logout();
    }, []);

    const { userData } = useContext(MainLayoutContext);
    const { toggleMode } = useDarkModeStore()

    return (
        <Fragment>

            <Stack
                flex={1}
                flexDirection="row"
                alignItems="center"
                pl={1}
                pr={0.5}
            >
                <Box>
                    {isMediumDown ?
                        <OpenBarButton />
                        :
                        <ToggleBarButton />
                    }
                </Box>

                <Box flex={1} />

                <Box>
                    <IconButton sx={{ ml: 1 }} onClick={toggleMode} color="inherit">
                        {theme.palette.mode === 'dark' ? <MdDarkMode /> : <MdLightMode />}
                    </IconButton>
                </Box>

                {/* Line */}
                <Box sx={{ height: 0.8, width: '1px', bgcolor: 'rgba(255,255,255,0.25)', mx: 1 }} />

                {/* Profile Panel */}
                <Stack
                    onClick={openMenu}
                    sx={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        px: 2,
                        py: 0,
                        borderRadius: 1,
                        cursor: 'pointer',
                        ':hover': {
                            bgcolor: 'rgba(0,0,0,0.15)',
                        }
                    }}
                >
                    <Stack
                        alignItems="flex-end"
                        mr={2}
                    >
                        <Box sx={{ color: 'white', mb: -0.5, whiteSpace: 'nowrap', fontWeight: 300, fontSize: '1rem' }}>
                            Welcome, {userData ? `${userData.firstName} ${userData.lastName}` : '-'}
                        </Box>
                        <Box sx={{ color: 'white', fontWeight: 500, fontSize: '0.9rem' }}>
                            Role : {userData ? userData?.roleName : '-'}
                        </Box>
                    </Stack>
                    <Stack>
                        <FaUserCircle size={34} color="white" />
                    </Stack>
                </Stack>
            </Stack>

            {/* TopberMenu */}
            <Popover
                open={Boolean(anchorEl)}
                anchorEl={anchorEl}
                onClose={closeMenu}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
            >
                <Stack py={1.5} px={0.5} width={250}>

                    <Divider />

                    <ListItem button onClick={onLogout}>
                        <Stack direction="row" alignItems="center" justifyContent="center" width={1}>
                            <MdLogout size={20} />
                            <Box sx={{ ml: 1.25 }}>LOG OUT</Box>
                        </Stack>
                    </ListItem>

                    <Divider />

                </Stack>
            </Popover>

        </Fragment>
    );
}
