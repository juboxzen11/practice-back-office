'use client'
import { useTokenAuthentication } from "@/api"
import { Account } from "@/api/services/account"
import { RootLayoutContext } from "@/app/layout"
import { authSelector, useAuthStore } from "@/features/auth/store"
import { useAuthorization } from "@/features/auth/useAuthorization"
import { Sidebar } from "@/features/sidebar/Sidebar"
import { useSidebarMarginLeft } from "@/features/sidebar/useSidebarMarginLeft"
import { useTheme } from "@emotion/react"
import { Box, IconButton, Stack } from "@mui/material"
import { useQuery } from "@tanstack/react-query"
import { Fragment, createContext, useContext, useMemo, useRef } from "react"
import { MdDarkMode, MdLightMode } from 'react-icons/md'
import { SidebarBody } from "./SidebarBody"
import { TopbarBody } from "./TopbarBody"

const topbarHeight = 50;
const bodyMinWidth = 320;
const topbarMinWidth = 320;
const fullbarWidth = 280;
const minibarWidth = 75;

export default ({ children }) => {
    const token = useAuthStore(authSelector.token);
    useTokenAuthentication({ accessToken: token });
    const { isAllowed } = useAuthorization({ redirectTo: '/login' });
    return isAllowed ? <Layout>{children}</Layout> : <Fragment />
}

export const MainLayoutContext = createContext({
    mainRef: null,
    userData: null,
});

const Layout = ({ children }) => {

    const mainRef = useRef(null);
    const sidebarMarginLeft = useSidebarMarginLeft({fullbarWidth, minibarWidth});

    const { data } = useQuery({ queryKey: ['account-me'], queryFn: Account.Me });
    const userData = useMemo(() => data?.payload || null, [data]);

    return (
        <MainLayoutContext.Provider value={{ mainRef, userData }}>
            <Box sx={{ display: 'flex', flexDirection: 'row', flex: 1 }}>

                <Sidebar fullbarWidth={fullbarWidth} minibarWidth={minibarWidth}>
                    <SidebarBody fullbarWidth={fullbarWidth} />
                </Sidebar>

                {/* Topbar */}
                <Box sx={{ position: 'fixed', top: 0, left: 0, width: 1, zIndex: 1, topbarMinWidth }}>
                    <Stack sx={{
                        transition: `margin 150ms ease-in-out, transform 500ms ease-in`,
                        ml: sidebarMarginLeft + 'px',
                        backgroundColor: 'primary.main',
                        boxShadow: '0px 2px 5px 2px rgba(0,0,0,0.1)',
                        height: topbarHeight,
                    }}>
                        <TopbarBody />
                    </Stack>
                </Box>

                {/* Body */}
                <Box sx={{ flex: 1, position: 'relative', height: '100vh', minWidth: bodyMinWidth }}>
                    <Box sx={{ display: 'flex', flexDirection: 'column', position: 'absolute', width: 1, height: 1}}>
                        <Box ref={mainRef} sx={{ flex: 1, mt: topbarHeight + 'px', overflow: 'auto' }}>
                            {children}
                        </Box>
                    </Box>
                </Box>
            </Box>
        </MainLayoutContext.Provider>
    )
}