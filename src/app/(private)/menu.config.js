import { FaClipboardList, FaUser } from 'react-icons/fa';

export const MenuList = [
    {
        label: "Customer",
        icon: FaUser,
        path: '/customer',
        activeMapping: ["/customer/*"], // หลังจากเข้ามาถึง path นี้ จะ include path ต่อๆไปเข้ามาหลัง /* path ทั้งหมด 
    },
    {
        label: "Transaction",
        icon: FaClipboardList,
        path: '/transaction',
        activeMapping: ["/transaction/*"],
    },
    {
        label: "Report",
        icon: FaClipboardList,
        path: '/report',
        activeMapping: ["/report/*"],
    },
    {
        label: "Privilege",
        icon: FaClipboardList,
        path: '/privilege',
        activeMapping: ["/privilege/*"],
    }
];