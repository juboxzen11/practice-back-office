'use client'
import { Box } from '@mui/material';

export default function Index() {
	return (
		<Box
			sx={{
				display: 'flex',
				width: '100%',
				alignItems: 'center',
				justifyContent: 'center',
				bgcolor: 'background.default',
				color: 'text.primary',
				borderRadius: 1,
				p: 3,
			}}
		>
			Index
		</Box>
	);
}
