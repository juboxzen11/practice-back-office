import pj from '@/../package.json';
import { sidebarSelector, useSidebarStore } from "@/features/sidebar/store";
import { Box, Collapse, Divider, ListItem, Stack, useMediaQuery, useTheme } from "@mui/material";
import { grey } from "@mui/material/colors";
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { Fragment, useCallback, useContext, useMemo, useState } from "react";
import { FaCaretLeft } from "react-icons/fa";
import { MainLayoutContext } from "./layout";
import { MenuList } from "./menu.config";

export const SidebarBody = ({ fullbarWidth }) => {
    const theme = useTheme();
    const asPath = usePathname();
    const [currentExpand, setCurrentExpand] = useState(null);
    const isMediumUp = useMediaQuery(theme.breakpoints.up('md'));
    const isMediumDown = useMediaQuery(theme.breakpoints.down('md'));
    const isHoverBar = useSidebarStore(sidebarSelector.isHoverBar);
    const isFullBar = useSidebarStore(sidebarSelector.isFullBar);
    const isMiniBar = useSidebarStore(sidebarSelector.isMiniBar);

    const compareActivePath = useCallback((mapping = []) => {
        return mapping.some((mappingPath) => {
            if (mappingPath.endsWith("/*")) {
                return asPath.startsWith(mappingPath.slice(0, -2));
            }
            return asPath == mappingPath;
        });
    }, [asPath])

    const mainLayout = useContext(MainLayoutContext)
    const { userData } = mainLayout
    const { roleId, permissionPaths = [] } = userData || {}
    const permissionPath = useCallback((mapping = []) => {
        if (roleId == 1)
            return true;
        return permissionPaths.some(path => {
            return mapping.some((mappingPath) => {
                if (mappingPath.endsWith("/*")) {
                    return path.startsWith(mappingPath.slice(0, -2));
                }
                return path == mappingPath;
            })
        })
    }, [permissionPaths])

    const showLabel = useMemo(
        () => (isMediumDown || isFullBar || isHoverBar)
        , [isMediumDown, isFullBar, isHoverBar]);

    const showIconLogo = useMemo(
        () => !(isMediumDown || isFullBar || isHoverBar)
        , [isMediumDown, isFullBar, isHoverBar]);

    return (
        <Fragment>
            {/* LOGO */}
            <Link href={"/"} passHref>
                <Stack mt={1} height={100} justifyContent="center" alignItems="center">
                    <Box sx={{ fontSize: showIconLogo ? '2rem' : '2.5rem', fontWeight: 300, color: 'primary.main', fontFamily: 'Garamond, serif' }}>
                        {showIconLogo ? "L" : "LOGO"}
                    </Box>
                    <Box sx={{ mt: -1.25, letterSpacing: 2, color: 'grey.700', fontSize: '0.75rem', fontWeight: 600, whiteSpace: 'nowrap' }}>
                        {showIconLogo ? "***" : "BACKOFFICE"}
                    </Box>
                </Stack>
            </Link>

            {/* LINE */}
            <Box sx={{ mt: 1, width: 1, height: '1px', bgcolor: 'grey.300' }} />

            {/* MENU SECTION */}
            <Box flex={1} width={fullbarWidth}>
                {MenuList.map((menu, i) => {

                    const isActive = compareActivePath(menu.activeMapping);
                    const allow = permissionPath(menu.activeMapping);

                    if (allow && menu.path) {
                        return (
                            <Fragment key={i}>
                                <Link href={menu.path} passHref>
                                    <MenuItem item={menu} isActive={isActive} showLabel={showLabel} />
                                </Link>
                                <Divider />
                            </Fragment>
                        );
                    }

                    //has submenu
                    if (menu.subMenu?.length) {

                        const expanded = currentExpand === i;
                        const defaultExpanded = currentExpand === null && isActive;
                        const isExpand = showLabel && (expanded || defaultExpanded);

                        return (
                            <Fragment key={i}>
                                <Box
                                    onClick={() => setCurrentExpand(prev => !expanded ? i : -1)}
                                    sx={{ position: 'relative' }}
                                >
                                    <MenuItem item={menu} isActive={isActive} showLabel={showLabel} />
                                    <Box sx={{
                                        color: isExpand ? 'grey.800' : 'grey.400',
                                        position: 'absolute',
                                        right: 5,
                                        top: 16
                                    }}>
                                        <FaCaretLeft
                                            style={{
                                                transform: `rotate(${isExpand ? '-90deg' : '0deg'})`,
                                                transition: 'transform 250ms'
                                            }}
                                        />
                                    </Box>
                                    <Divider />
                                </Box >
                                <Fragment>
                                    <Collapse
                                        in={isExpand}
                                    >
                                        {menu.subMenu.map((subMenu, j) => {
                                            const isActive = compareActivePath(subMenu.activeMapping);
                                            const allow = permissionPath(subMenu.activeMapping);
                                            if (allow)
                                                return (
                                                    <Link key={j} href={subMenu.path} passHref>
                                                        <MenuItem sx={{ pl: 5 }} item={subMenu} isActive={isActive} showLabel={showLabel} isSubmenu />
                                                    </Link>
                                                )
                                        })}
                                    </Collapse>
                                    {isExpand && <Divider />}
                                </Fragment>
                            </Fragment>
                        )
                    }
                })}
            </Box>

            {/* LINE */}
            <Box sx={{ my: 1, width: 1, height: '1px', bgcolor: grey[300] }} />
            <Box sx={{
                textAlign: 'center',
                fontSize: '0.85rem',
                color: 'grey.600',
                fontWeight: 600,
                pb: 1,
                px: 2,
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
            }}>
                v{pj.version} - backend
            </Box>

        </Fragment>
    )
}

export const MenuItem = ({
    item,
    isActive,
    showLabel,
    isSubmenu = false
}) => {

    return (
        <Stack flexDirection="row">

            {isSubmenu ?
                <Box sx={{
                    bgcolor: isActive ? 'primary.main' : 'grey.300',
                    width: isActive ? '3px' : '1px',
                }} />
                :
                <Box sx={{
                    bgcolor: isActive ? 'primary.main' : 'white',
                    width: isActive ? '3px' : '1px',
                }} />
            }

            <ListItem button sx={{ width: 1, padding: 0 }}>
                <Stack sx={{
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                    py: 1.5,
                    px: 2,
                }}>
                    {item.icon &&
                        <Box display="flex" color={isActive ? 'primary.main' : 'grey.800'} px={1}>
                            <item.icon size={24} />
                        </Box>
                    }
                    {showLabel &&
                        <Box sx={{
                            color: isActive ? 'primary.main' : 'grey.800',
                            fontWeight: 300,
                            fontSize: '1.1rem',
                            //lineHeight: '1rem',
                            ml: 2,
                            textAlign: "left",
                            wordBreak: 'break-word',
                        }}>
                            {item.label}
                        </Box>
                    }
                </Stack>
            </ListItem>
        </Stack>
    );
}