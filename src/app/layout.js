'use client'
import { COLOR_MODE, useDarkModeStore } from '@/features/dark-mode/store';
import { CssBaseline, ThemeProvider, createTheme } from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { MutationCache, QueryCache, QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { useMemo } from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function RootLayout({ children }) {
    
    const queryClient = new QueryClient()
    const { mode } = useDarkModeStore()
    const theme = useMemo(() => (
        createTheme({
            palette: {
                mode,
                primary: {
                    main: mode == COLOR_MODE.LIGHT ? '#1976d2' : "#1976d2"
                },
                background: {
                    default: mode == COLOR_MODE.LIGHT ? '#FFF' : '#333'
                }
            },
            typography: {
                fontFamily: 'Prompt',
            }
        })
    ), [mode]);

    return (
        <html lang="en">
            <body>
                <QueryClientProvider client={queryClient}>
                    <ThemeProvider theme={theme}>
                        <LocalizationProvider dateAdapter={AdapterMoment}>
                            <ToastContainer toastify={{ position: "top-right", autoClose: 3000, theme: "colored", limit: 3 }} />
                            <CssBaseline />
                            {children}
                        </LocalizationProvider>
                    </ThemeProvider>
                </QueryClientProvider>
            </body>
        </html>
    )
}

//react-query config
const clientOptions = () => {
    // global call api error
    var errorShowLimited = false;
    var errorShowLimitedTimer = null;
    const onError = (error) => {
        const status = error?.response?.status || 0;
        const message = error?.response?.statusText || error?.message || "";

        // if(!errorShowLimited){
        //     errorShowLimited = true
        //     toast.error(`[${status}] ${message}`, {autoClose: 3000});
        // }

        // clearTimeout(errorShowLimitedTimer)
        // errorShowLimitedTimer = setTimeout(() => {
        //     errorShowLimited = false
        // }, 3000);
    }

    return {
        defaultOptions: {
            queries: {
                refetchOnWindowFocus: false,
                retry: false
            }
        },
        queryCache: new QueryCache({
            onError,
        }),
        mutationCache: new MutationCache({
            onError
        })
    }
}
const queryClient = new QueryClient(clientOptions())